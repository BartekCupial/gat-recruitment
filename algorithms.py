from typing import Tuple

import numpy as np


def policy_evaluation(P, nS: int, policy: np.ndarray, gamma: float = 0.9, eps: float = 1e-3) -> np.ndarray:
	"""
	Calculates expected return for each state, given that we follow actions from policy.
	We use assumption that the world is Markovian and find Value function with DP. 
	Could also be solved analytically (V = (I - gammaP)^{-1} * R) or with simulation.

	Args:
		P: State transitions matrix.
		nS (int): Number of states.
		policy (np.ndarray): Policy we want to evaluate.
		gamma (float, optional): Discount factor. Defaults to 0.9.
		eps (float, optional): Defines convergence. Defaults to 1e-3.

	Returns:
		(np.ndarray): Value function.
	"""
	prev_value_function = np.ones(nS)
	value_function = np.zeros(nS)

	while np.max(np.abs(prev_value_function - value_function)) > eps:
		prev_value_function = value_function
		value_function = np.zeros(nS)
		for state in range(nS): 
			for probability, next_state, reward, _ in P[state][policy[state]]:
				value_function[state] += probability * (reward + gamma * prev_value_function[next_state])

	return value_function


def policy_improvement(P, nS: int, nA: int, value_function: np.ndarray, gamma: float = 0.9) -> np.ndarray:
	"""
	Given the value function improve the policy.

	Args:
		P: State transitions matrix.
		nS (int): Number of states.
		nA (int): Number of actions.
		value_function (np.ndarray): Value function.
		gamma (float, optional): Discount factor. Defaults to 0.9.

	Returns:
		(np.ndarray): An array of integers. Each integer is the optimal action to take 
		in that state according to the environment dynamics and the given value function.
	"""
	new_policy = np.zeros(nS, dtype=int)

	for state in range(nS):
		policy_for_state = np.zeros(nA)
		for action in range(nA):
			for probability, next_state, reward, _ in P[state][action]:
				policy_for_state[action] += probability * (reward + gamma * value_function[next_state])

		new_policy[state] = np.argmax(policy_for_state)

	return new_policy


def policy_iteration(P, nS: int, nA: int, gamma: float = 0.9, eps: float = 10e-3) -> Tuple[np.ndarray, np.ndarray]:
	"""
	Finds optimal value function and policy by running policy evaluation and policy iteration.

	Args:
		P: State transitions matrix.
		nS (int): Number of states.
		nA (int): Number of actions.
		gamma (float, optional): Discount factor. Defaults to 0.9.
		eps (float, optional): Defines convergence. Defaults to 10e-3.

	Returns:
		Tuple[np.ndarray, np.ndarray]: Final value function and policy.
	"""
	old_policy = np.zeros(nS, dtype=int)
	value_function = policy_evaluation(P, nS, old_policy, gamma, eps)
	policy = policy_improvement(P, nS, nA, value_function, gamma)
	
	# L1 norm
	while ~np.all(policy==old_policy):
		value_function = policy_evaluation(P, nS, policy, gamma, eps)
		old_policy = policy
		policy = policy_improvement(P, nS, nA, value_function, gamma)

	return value_function, policy


def value_iteration(P, nS: int, nA: int, gamma: float = 0.9, eps: float = 1e-3):
	"""
	Finds optimal value function and policy by finding optimal value function 
	and than extracting policy from it.

	Args:
		P: State transitions matrix.
		nS (int): Number of states.
		nA (int): Number of actions.
		gamma (float, optional): Discount factor. Defaults to 0.9.
		eps (float, optional): [description]. Defaults to 10e-3.

	Returns:
		Tuple[np.ndarray, np.ndarray]: Final value function and policy.
	"""
	prev_value_function = np.zeros(nS, dtype=np.float64)
	value_function = np.zeros(nS, dtype=np.float64)
	policy = np.zeros(nS, dtype=int)

	# finding optimal value function
	for _ in range(100):
		for state in range(nS): 
			state_value_function = np.zeros(nS)
			for action in range(nA):
				for probability, next_state, reward, _ in P[state][action]:
					state_value_function[action] += probability * (reward + gamma * prev_value_function[next_state])
			value_function[state] = np.max(state_value_function)

		if np.max(np.abs(value_function - prev_value_function)) < eps:
			break

		prev_value_function = value_function.copy()

	# one policy update (extract policy from the optimal value function)
	for state in range(nS):
		state_policy = np.zeros(nA)
		for action in range(nA):
			for probability, next_state, reward, _ in P[state][action]:
				state_policy[action] += probability * (reward + gamma * value_function[next_state])
		policy[state] = np.argmax(state_policy)

	return value_function, policy

