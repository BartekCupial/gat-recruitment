import time

from argparse import ArgumentParser
from typing import Callable

import numpy as np
import gym

from algorithms import policy_iteration, value_iteration


def play_episode(env: gym.Env, policy: np.ndarray, max_steps: int = 20, render: bool = True) -> float:
    episode_reward = 0
    observation = env.reset()

    for _ in range(max_steps):
        if render: 
            env.render()
            time.sleep(0.25)

        action = policy[observation]
        observation, reward, done, _ = env.step(action)
        episode_reward += reward

        if done:
            break

    if render: 
        env.render()

    return episode_reward
    

def solve(
    map_name: str = "8x8", 
    is_slippery: bool = True, 
    render: bool = True, 
    num_runs: int = 1, 
    gamma: float = 0.9, 
    eps: float = 1e-3
):
    env = gym.make(f"FrozenLake8x8-v0", map_name=map_name, is_slippery=is_slippery)

    def test_algorithm(algorithm: Callable) -> float:
        print(f"{algorithm.__name__}:")
        start = time.time()
        _, policy = algorithm(env.P, env.nS, env.nA, gamma=gamma, eps=eps)
        end = time.time()
        print(f"solving time: {end - start}")

        start = time.time()
        if num_runs == 1:
            average_reward = play_episode(env, policy, max_steps=100, render=render)
        else:
            rewards = []
            for _ in range(num_runs):
                episode_reward = play_episode(env, policy, max_steps=100, render=False)
                rewards.append(episode_reward)
            average_reward = np.mean(rewards)
        end = time.time()
        print(f"average reward: {average_reward}")
        print(f"playing time: {end - start:.2f}s")

    test_algorithm(policy_iteration)
    test_algorithm(value_iteration)
    


def parse_cli():
    parser = ArgumentParser(
        description="""
        Solve Frozen Lake environment with policy iteration or value iteration.
        """
    )
    parser.add_argument(
        "--map-name",
        type=str,
        required=False,
        default="8x8",
        help="Size of FrozenLake-v0 environment [8x8 or 4x4].",
    )
    parser.add_argument(
        "--is-slippery",
        type=bool,
        required=False,
        default=False,
        help="Controls if the environment is deterministic or stochastic.",
    )
    parser.add_argument(
        "--render",
        type=bool,
        required=False,
        default=False,
        help="Renders the environment.",
    )
    parser.add_argument(
        "--num-runs",
        type=int,
        required=False,
        default=1,
        help="Number of test runs to evaluate time and number of solver trials.",
    )
    parser.add_argument(
        "--gamma",
        type=float,
        required=False,
        default=0.9,
        help="Discount factor for future rewards.",
    )
    parser.add_argument(
        "--eps",
        type=float,
        required=False,
        default=1e-3,
        help="Defines convergence.",
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_cli()
    solve(**vars(args))