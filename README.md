# ML engineer test for Global App Testing

## FrozenLake8x8-v0 gym environment.
FrozenLake is a simple environment where agent can walk on small grid. The agent is rewarded for finding a walkable path to a goal line. Game is over if agent walks into a hole. Game can be played deterministically or with partially random movements (**is_slippery** = **True** / **False**).

### Observations: 
* We know all information about this world (transition model and rewards for visiting the states), whis can be accessed with variable **P**. 

* The world is Markov, which means that we can focus just on current state and we don't need to take history into account.

* In stochastic setting it is not always possible to reach the goal (we could misstep and fall into a hole).

* State space **nS** = 64 for 8x8 environment and **nS** = 16 for 4x4 one.

* Action space **nA** = 4 (left, up, right, down).

### Solution
Given the state space size and the model of the world the most suitable solution for me was to use dynamic programming, namely policy iteration and value iteration algorithms. They both yield optimal policies and because our state space is reasonably small we can be sure that algorithms will terminate in reasonable time.
I've implemented both algorithms to compare the conversion time. 

### Results
Results of experiments can be found in results.ipynb. 
TLDR;
* value iteration finds solution faster than policy iteration in both **is_slippery** = **True** and **False**.
* for stochastinc movements environment is solved in 0.5668%. 
* for deterministic movements environment is solved in 100%.

### SETUP
To install required dependencies simply run. Tested with python version 3.8.11.
```
pip install -r requirements.txt
```

### RUN 
More information about how to run script inside run module in ArgumentParser.
Example:
```
python -m run --render True --is-slippery True
```